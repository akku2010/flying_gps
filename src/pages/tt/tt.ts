import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
// import { ThemeableBrowser, ThemeableBrowserOptions } from '@ionic-native/themeable-browser';
var iabRef = null

@IonicPage()
@Component({
  selector: 'page-tt',
  templateUrl: 'tt.html',
})
export class TtPage {
  data: {};
  resp: any;
  islogin: any;
  iabRef = null;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public apiservice: ApiServiceProvider,public alerCtrl: AlertController,
    // private themeableBrowser: ThemeableBrowser
    ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    // this.payment();

  }

  insertMyHeader() {
    iabRef.executeScript({
      code: "var b=document.querySelector('body'); var a=document.createElement('div');document.createTextNode('my own header!'); a.appendChild(newContent);b.parentNode.insertBefore(a,b);"
    }, function () {
      alert("header successfully added");
    });
  }

  // iabClose() {
  //   iabRef.removeEventListener('exit', this.iabClose);
  // }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad TtPage');
  // }

  // call() {
  //   if (this.resp == undefined) {
  //     let alerttemp = this.alerCtrl.create({
  //       message: "invalid-args",
  //       buttons: [{
  //         text: 'Okay'
  //       }]
  //     });
  //     alerttemp.present();
  //   } else {
  //     const options: ThemeableBrowserOptions = {
  //       statusbar: {
  //         color: '#f54278'
  //       },
  //       toolbar: {
  //         height: 50,
  //         color: '#da4407'
  //       },
  //       title: {
  //         color: '#ffffff',
  //         showPageTitle: true
  //       },
  //       backButton: {
  //         wwwImage: '/assets/icon/arrow.png',
  //         imagePressed: 'back_pressed',
  //         align: 'left',
  //         // event: 'backPressed'
  //         event: 'closePressed'
  //       },
  //       backButtonCanClose: true
  //     };

  //     // const browser: ThemeableBrowserObject = this.themeableBrowser.create(this.resp, '_self', options);
  //     this.themeableBrowser.create(this.resp, '_self', options);
  //   }
  // }

  // payment() {
  //   console.log("inside the payment");
  //   var url = "https://www.oneqlik.in/pullData/getSetuLink?mobileNumber=" + this.islogin.phn;
  //   this.apiservice.startLoading().present();
  //   this.apiservice.getdevicesForAllVehiclesApi(url)
  //     .subscribe(resp => {
  //       this.apiservice.stopLoading();
  //       console.log("server image url=> ", resp);
  //       console.log("chk response", resp);
  //       this.resp = resp.link
  //       console.log("final link", this.resp);
  //       if (this.resp == undefined) {
  //         let alerttemp = this.alerCtrl.create({
  //           message: "invalid-args",
  //           buttons: [{
  //             text: 'Okay'
  //           }]
  //         });
  //         alerttemp.present();
  //       }
  //     },
  //       () => {
  //         this.apiservice.stopLoading();
  //       });
  //   this.apiservice.payment(this.data);
  // }

}
