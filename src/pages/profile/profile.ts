import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, Events, Platform, ActionSheetController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { IonPullUpFooterState } from 'ionic-pullup';
import { AppVersion } from '@ionic-native/app-version';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { FileTransferObject, FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer';
import { SocketConnectionProvider } from '../../providers/socket-connection/socket-connection';
// import * as io from 'socket.io-client';
// import { TextToSpeech } from '@ionic-native/text-to-speech';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  islogin: any;
  token: string;
  aVer: any;
  isDealer: boolean;
  footerState: IonPullUpFooterState;
  credentialsForm: FormGroup;
  fileUrl: string;


  constructor(
    private appVersion: AppVersion,
    public apiCall: ApiServiceProvider,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private formBuilder: FormBuilder,
    // private storage: Storage,
    public platform: Platform,
    private actionSheetCtrl: ActionSheetController,
    private crop: Crop,
    private camera: Camera,
    private transfer: FileTransfer,
    private toastCtrl: ToastController,
    private socketConn: SocketConnectionProvider
  ) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
   
    this.appVersion.getVersionNumber().then((version) => {
      this.aVer = version;
    });
    this.isDealer = this.islogin.isDealer;

    this.footerState = IonPullUpFooterState.Collapsed;

    this.credentialsForm = this.formBuilder.group({
      fname: [this.islogin.fn],
      lname: [this.islogin.ln],
      email: [this.islogin.email],
      phonenum: [this.islogin.phn],
      org: [this.islogin._orgName],
      // dealer: []
    });

    this.getImgUrl();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter ProfilePage');
  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

  getImgUrl() {
    let url = "https://www.oneqlik.in/users/shareProfileImage?uid=" + this.islogin._id;
   
    this.apiCall.getdevicesForAllVehiclesApi(url)
      .subscribe(resp => {
        if (JSON.parse(JSON.stringify(resp)).imageDoc.length > 0) {
          let imgUrl = JSON.parse(JSON.stringify(resp)).imageDoc[0];
          let str1 = imgUrl.split('public/');
          this.fileUrl = "https://www.oneqlik.in/" + str1[1];
        }
      });
  }

  public selectImage() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.SAVEDPHOTOALBUM);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.pickImage(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  pickImage(sourceType) {
    let url = "https://www.oneqlik.in/users/uploadProfilePicture";
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.crop.crop(imageData, { quality: 100 })
        .then(
          newImage => {
            let dlink123 = newImage.split('?');
            let wear = dlink123[0];
            const fileTransfer: FileTransferObject = this.transfer.create();
            const uploadOpts: FileUploadOptions = {
              fileKey: 'photo',
              fileName: wear.substr(wear.lastIndexOf('/') + 1)
            };
            this.toggleFooter();
            this.apiCall.startLoading().present();

            fileTransfer.upload(wear, url, uploadOpts)
              .then((data) => {
                this.apiCall.stopLoading();
                let respData = data.response;
                this.dlUpdate(respData);
              }, (err) => {
                this.apiCall.stopLoading();
                console.log(err);
                this.toastCtrl.create({
                  message: 'Something went wrong while uploading file... Please try after some time..',
                  duration: 2000,
                  position: 'bottom'
                }).present();
              });
          });
    }, (err) => {
      console.log("imageData err: ", err)
      // Handle error
    });
  }

  dlUpdate(dllink) {
    // let dlink123 = dllink.split('?');
    // let wear = dlink123[0];
    let _burl = "https://www.oneqlik.in/users/updateImagePath";
    let payload = {
      imageDoc: [dllink],
      _id: this.islogin._id
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(_burl, payload)
      .subscribe(() => {
        this.apiCall.stopLoading();
        this.getImgUrl();
      },
        err => {
          this.apiCall.stopLoading();
        });
  }

  reports() {
    this.navCtrl.push('ReportsCollectionPage');
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

  settings() {
    this.navCtrl.push('SettingsPage');
  }

  service() {
    this.navCtrl.push(ServiceProviderPage, {
      param: this.islogin
    })
  }

  password() {
    this.navCtrl.push(UpdatePasswordPage, {
      param: this.islogin
    })
  }

  onSignIn() {
    let data = {
      "fname": this.credentialsForm.value.fname,
      "lname": this.credentialsForm.value.lname,
      "org": this.credentialsForm.value.org,
      "noti": true,
      "uid": this.islogin._id,
      "fuel_unit": "LITRE"
    }
    this.apiCall.startLoading().present();
    this.apiCall.updateprofile(data)
      .subscribe(resdata => {
        this.apiCall.stopLoading();
        if (resdata.token) {
          let alert = this.alertCtrl.create({
            message: 'Profile updated succesfully!',
            buttons: [{
              text: 'OK',
              handler: () => {
                let logindata = JSON.stringify(resdata);
                let logindetails = JSON.parse(logindata);
                let userDetails = window.atob(logindetails.token.split('.')[1]);
                let details = JSON.parse(userDetails);
                localStorage.setItem("loginflag", "loginflag");
                localStorage.setItem('details', JSON.stringify(details));
                localStorage.setItem('condition_chk', details.isDealer);
                this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                this.footerState = 1;
                this.toggleFooter();
              }
            }]
          });
          alert.present();
        }
      },
        err => {
          this.apiCall.stopLoading();
          console.log("error from service=> ", err)
        });
  }



  logout() {
    if(localStorage.getItem('getDevicesInterval_ID')) {
      let intervalid = localStorage.getItem('getDevicesInterval_ID');
      clearInterval(JSON.parse(intervalid));
    }
    this.token = localStorage.getItem("DEVICE_TOKEN");
    let pushdata = {};
    if (this.platform.is('android')) {
      pushdata = {
        "uid": this.islogin._id,
        "token": this.token,
        "os": "android"
      }
    } else {
      pushdata = {
        "uid": this.islogin._id,
        "token": this.token,
        "os": "ios"
      }
    }

    let alert = this.alertCtrl.create({
      message: 'Do you want to logout from the application?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.apiCall.startLoading().present();
          this.apiCall.pullnotifyCall(pushdata)
            .subscribe(() => {
              this.apiCall.stopLoading();
             
              localStorage.clear();
              localStorage.setItem('count', null)
              this.socketConn.socketDisconnect();
              this.socketConn.notifSocketDisconnect();
              // this.storage.clear().then(() => {
              //   console.log("ionic storage cleared!")
              // })
              this.navCtrl.setRoot('LoginPage');

            },
              err => {
                this.apiCall.stopLoading();
                console.log(err)
              });
        }
      },
      {
        text: 'No',
        handler: () => {
          // this.menuCtrl.close();
        }
      }]
    });
    alert.present();
  }
}

@Component({
  templateUrl: './service-provider.html',
  selector: 'page-profile'
})

export class ServiceProviderPage {
  uData: any = {};
  sorted: any = [];
  constructor(public navParam: NavParams) {
    this.uData = this.navParam.get("param");
    if (this.uData.Dealer_ID != undefined) {
      this.sorted = this.uData.Dealer_ID;
    } else {
      this.sorted.first_name = this.uData.fn;
      this.sorted.last_name = this.uData.ln;
      this.sorted.phone = this.uData.phn;
      this.sorted.email = this.uData.email;
    }
    console.log("udata=> ", this.uData)
    console.log("udata=> ", JSON.stringify(this.uData))
  }
}

@Component({
  selector: 'page-profile',
  templateUrl: './update-password.html'
})

export class UpdatePasswordPage {
  passData: any;
  cnewP: any;
  newP: any;
  oldP: any;
  constructor(
    public navParam: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public apiSrv: ApiServiceProvider,
    public navCtrl: NavController
  ) {
    this.passData = this.navParam.get("param");
    console.log("passData=> " + JSON.stringify(this.passData))
  }

  savePass() {
    if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
      let alert = this.alertCtrl.create({
        message: 'Fields should not be empty!',
        buttons: ['OK']
      });
      alert.present();
    } else {
      if (this.newP != this.cnewP) {
        let alert = this.alertCtrl.create({
          message: 'Password Missmatched!!',
          buttons: ['Try Again']
        });
        alert.present();
      } else {
        var data = {
          "ID": this.passData._id,
          "OLD_PASS": this.oldP,
          "NEW_PASS": this.newP
        }
        this.apiSrv.startLoading().present();
        this.apiSrv.updatePassword(data)
          .subscribe(respData => {
            this.apiSrv.stopLoading();
            console.log("respData=> ", respData)
            const toast = this.toastCtrl.create({
              message: 'Password Updated successfully',
              position: "bottom",
              duration: 2000
            });
            toast.onDidDismiss(() => {
              this.oldP = "";
              this.newP = "";
              this.cnewP = "";
            });
            toast.present();
          },
            err => {
              this.apiSrv.stopLoading();
              console.log("error in update password=> ", err)
              // debugger
              if (err.message == "Timeout has occurred") {
                // alert("the server is taking much time to respond. Please try in some time.")
                let alerttemp = this.alertCtrl.create({
                  message: "the server is taking much time to respond. Please try in some time.",
                  buttons: [{
                    text: 'Okay',
                    handler: () => {
                      this.navCtrl.setRoot("DashboardPage");
                    }
                  }]
                });
                alerttemp.present();
              } else {
                const toast = this.toastCtrl.create({
                  message: err._body.message,
                  position: "bottom",
                  duration: 2000
                });
                toast.onDidDismiss(() => {
                  this.oldP = "";
                  this.newP = "";
                  this.cnewP = "";
                  this.navCtrl.setRoot("DashboardPage");
                });
                toast.present();
              }
            });
      }
    }

  }
}
