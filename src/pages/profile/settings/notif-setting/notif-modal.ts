import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../../../providers/api-service/api-service';

@Component({
    selector: 'app-notif-modal',
    templateUrl: './notif-modal.html'
})
export class NotifModalPage implements OnInit {
    isAddEmail: boolean = false;
    isAddPhone: boolean = false;
    emailList: any = [];
    phonelist: any = [];
    notifType: any;
    islogin: any;
    data: any;
    femail: string = '';
    fphone: number = 0;
    model: any = {};
    newArray: any = [];
    constructor(
        public navParams: NavParams,
        private viewCtrl: ViewController,
        private apiCall: ApiServiceProvider,
        private toastCtrl: ToastController) {
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.notifType = navParams.get('notifData').notifType;
        this.data = navParams.get('notifData').compData;

        if (navParams.get('notifData').buttonClick == 'email') {
            this.isAddEmail = true;
        }
        if (navParams.get('notifData').buttonClick == 'phone') {
            this.isAddPhone = true;
        }

        if (navParams.get('notifData').newArray !== undefined) {
            console.log("check all data first: ", navParams.get('notifData').newArray)
            this.newArray = navParams.get('notifData').newArray;
        }
    }

    ngOnInit() {
        this.emailList = this.data[this.notifType].emails;
        this.phonelist = this.data[this.notifType].phones;
    }

    onSubmit() {
        console.log('Form submitted!!')
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
    fData: any = {};

    addEmail(id, index) {
        if (id == 'add') {
            if (!this.data[this.notifType].emails) {
                this.data[this.notifType].emails = [];
            }
            let arrayList = [];
            this.data[this.notifType].emails.push(this.model.email);
            console.log(this.data);
            let that = this;
            arrayList = this.newArray.map(function (r) {
                if (r.key === that.notifType) {
                    return r;
                }
            });
            console.log(arrayList)

            let filtered = arrayList.filter(function (x) {
                return x !== undefined;
            });

            console.log(filtered);
            let res123 = this.newArray.map(obj => filtered.find(o => o.key === obj.key) || obj);
            console.log(res123);
            let temp = [];
            for (let e = 0; e < res123.length; e++) {
                temp.push({
                    [res123[e].key]: res123[e].keys,
                    key: res123[e].key
                });
            }
            /* covert an array into object */
            let res = {};
            temp.forEach(function (a) { res[a.key] = a[a.key] })

            this.fData.contactid = this.islogin._id;
            this.fData.alert = res;
            let url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(respData => {
                    this.apiCall.stopLoading();
                    console.log("Please check: ", respData)
                    this.model.email = null;
                    if (respData) {
                        this.toastCtrl.create({
                            message: 'Email Id added!!',
                            duration: 1000,
                            position: 'middle'
                        }).present();
                    }
                },
                    err => {
                        this.apiCall.stopLoading();
                        this.toastCtrl.create({
                            message: 'Internal server error, please try again!!!',
                            duration: 1500,
                            position: 'bottom'
                        }).present();
                    });
        }
        if (id == 'delete') {
            let arrayList = [];
            var splicedmail = this.data[this.notifType].emails[index];
            console.log("spliceNumber =>", splicedmail)
            this.data[this.notifType].emails.splice(index, 1);
            ///////////////////////////////////////
            let that = this;
            arrayList = this.newArray.map(function (r) {
                if (r.key === that.notifType) {
                    return r;
                }
            });
            console.log(arrayList)

            let filtered = arrayList.filter(function (x) {
                return x !== undefined;
            });

            console.log(filtered);
            let res123;
            // filtered[0][this.notifType].emails.splice(index, 1);
            if (filtered.length > 0) {
                res123 = this.newArray.map(obj => filtered.find(o => o.key === obj.key) || obj);
                console.log(res123);
            } else {
                res123 = this.newArray.map(obj => arrayList.find(o => o.key === obj.key) || obj);
                console.log(res123);
            }
            let temp = [];
            for (let e = 0; e < res123.length; e++) {
                temp.push({
                    [res123[e].key]: res123[e].keys,
                    key: res123[e].key
                });
            }
            /* covert an array into object */
            let res = {};
            temp.forEach(function (a) { res[a.key] = a[a.key] })
            ///////////////////////////////////////
            this.fData.contactid = this.islogin._id;
            // this.fData.alert = this.data;
            this.fData.alert = res;
            let url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(respData => {
                    this.apiCall.stopLoading();
                    this.model.email = null;
                    console.log("Please check: ", respData)
                    if (respData) {
                        this.toastCtrl.create({
                            message: 'Email Id deleted!!',
                            duration: 1000,
                            position: 'middle'
                        }).present();
                    }
                },
                    err => {
                        this.apiCall.stopLoading();
                        this.toastCtrl.create({
                            message: 'Internal server error, please try again!!!',
                            duration: 1500,
                            position: 'bottom'
                        }).present();
                    });
        }
    }

    addPhone(id, index) {
        if (id == 'add') {
            if (!this.data[this.notifType].phones) {
                this.data[this.notifType].phones = [];
            }
            let arrayList = [];
            this.data[this.notifType].phones.push(this.model.phone);

            let that = this;
            arrayList = this.newArray.map(function (r) {
                if (r.key === that.notifType) {
                    return r;
                }
            });
            // console.log(arrayList)

            let filtered = arrayList.filter(function (x) {
                return x !== undefined;
            });

            console.log(filtered);
            let res123 = this.newArray.map(obj => filtered.find(o => o.key === obj.key) || obj);
            // console.log(res123);
            var temp = [];
            for (let e = 0; e < res123.length; e++) {
                temp.push({
                    [res123[e].key]: res123[e].keys,
                    key: res123[e].key
                });
            }
            /* covert an array into object */
            let res = {};
            temp.forEach(function (a) { res[a.key] = a[a.key] })


            this.fData.contactid = this.islogin._id;
            this.fData.alert = res;
            let url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(respData => {
                    this.apiCall.stopLoading();
                    console.log("Please check: ", respData)
                    this.model.phone = null;
                    if (respData) {
                        this.toastCtrl.create({
                            message: 'Phone number added!!',
                            duration: 1000,
                            position: 'middle'
                        }).present();
                    }
                },
                    err => {
                        this.apiCall.stopLoading();
                        this.toastCtrl.create({
                            message: 'Internal server error, please try again!!!',
                            duration: 1500,
                            position: 'bottom'
                        }).present();
                    });
        }
        if (id == 'delete') {
            var splicedNum = this.data[this.notifType].phones[index];
            console.log("spliceNumber =>", splicedNum)
            this.data[this.notifType].phones.splice(index, 1);
            ///////////////////////////////////////
            let arrayList = [];
            let that = this;
            arrayList = this.newArray.map(function (r) {
                if (r.key === that.notifType) {
                    return r;
                }
            });
            console.log(arrayList)

            let filtered = arrayList.filter(function (x) {
                return x !== undefined;
            });

            console.log(filtered);
            let res123;
            // filtered[0][this.notifType].emails.splice(index, 1);
            if (filtered.length > 0) {
                res123 = this.newArray.map(obj => filtered.find(o => o.key === obj.key) || obj);
                console.log(res123);
            } else {
                res123 = this.newArray.map(obj => arrayList.find(o => o.key === obj.key) || obj);
                console.log(res123);
            }
            let temp = [];
            for (let e = 0; e < res123.length; e++) {
                temp.push({
                    [res123[e].key]: res123[e].keys,
                    key: res123[e].key
                });
            }
            /* covert an array into object */
            let res = {};
            temp.forEach(function (a) { res[a.key] = a[a.key] })
            ///////////////////////////////////////
            this.fData.contactid = this.islogin._id;
            this.fData.alert = res;
            let url = this.apiCall.mainUrl + 'users/editUserDetails';
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(url, this.fData)
                .subscribe(respData => {
                    this.apiCall.stopLoading();
                    console.log("Please check: ", respData)
                    this.model.phone = null;
                    if (respData) {
                        this.toastCtrl.create({
                            message: 'Phone number deleted!!',
                            duration: 1000,
                            position: 'middle'
                        }).present();
                    }
                },
                    err => {
                        this.apiCall.stopLoading();
                        this.toastCtrl.create({
                            message: 'Internal server error, please try again!!!',
                            duration: 1500,
                            position: 'bottom'
                        }).present();
                    });
        }
    }

    Close() {
        // this.dialogRef.close();
    }
}