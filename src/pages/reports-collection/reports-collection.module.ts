import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportsCollectionPage } from './reports-collection';

@NgModule({
  declarations: [
    ReportsCollectionPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportsCollectionPage),
  ],
})
export class ReportsCollectionPageModule {}
