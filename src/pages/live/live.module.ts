import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LivePage } from './live';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LivePage
  ],
  imports: [
    IonicPageModule.forChild(LivePage),
    SelectSearchableModule,
    IonBottomDrawerModule,
    TranslateModule.forChild()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LivePageModule {}
