webpackJsonp([52],{

/***/ 625:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FastagListPageModule", function() { return FastagListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fastag_list__ = __webpack_require__(738);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var FastagListPageModule = /** @class */ (function () {
    function FastagListPageModule() {
    }
    FastagListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fastag_list__["a" /* FastagListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fastag_list__["a" /* FastagListPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], FastagListPageModule);
    return FastagListPageModule;
}());

//# sourceMappingURL=fastag-list.module.js.map

/***/ }),

/***/ 738:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__msg_utility_msg_utility__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FastagListPage = /** @class */ (function () {
    function FastagListPage(navCtrl, navParams, apiCall, toastCtrl, modalCtrl, formBuilder, navParam) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.navParam = navParam;
        this.fastagList = [];
        this.page = 1;
        this.DealerArraySearch = [];
        this.limit = 10000;
        this.CustomerArraySearch = [];
        this.DCustomerArraySearch = [];
        this.selectus = [];
        this.selectDealer = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.msgForm = this.formBuilder.group({
            title: [this.islogin.fn],
            msg: [this.islogin.ln]
        });
    }
    FastagListPage.prototype.ionViewDidEnter = function () {
        console.log('ionViewDidEnter FastagListPage');
    };
    FastagListPage.prototype.ngOnInit = function () {
        this.getDealer();
    };
    FastagListPage.prototype.addFastag = function () {
        this.navCtrl.push('FastagPage', {});
    };
    FastagListPage.prototype.getDealer = function () {
        var _this = this;
        this.page = 1;
        this.DealerArraySearch = [];
        this.apiCall.getDealers(this.islogin._id, this.page, this.limit)
            .subscribe(function (data) {
            _this.DealerArraySearch = data;
            _this.getcust();
        }, function (err) {
            console.log("getting error from server=> ", err);
            var toast = _this.toastCtrl.create({
                message: 'No Dealer(s) found',
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            // toast.onDidDismiss(() => {
            //   this.navCtrl.setRoot("DashboardPage");
            // })
            _this.getcust();
        });
    };
    FastagListPage.prototype.getcust = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getCustomer?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            _this.CustomerData = data;
            _this.CustomerArraySearch = [];
            _this.CustomerArraySearch = _this.CustomerData;
            _this.getAllDealers();
        }, function (err) {
            var a = JSON.parse(err._body);
            var b = a.message;
            var toast = _this.toastCtrl.create({
                message: b,
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            _this.getAllDealers();
        });
    };
    FastagListPage.prototype.getAllDealers = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getAllDealerVehicles?supAdmin=' + this.islogin._id;
        var toast = this.toastCtrl.create({
            message: 'Loading dealers..',
            position: 'bottom',
            duration: 1500
        });
        toast.present();
        this.selectDealer = [];
        this.apiCall.getAllDealerCall(baseURLp)
            .subscribe(function (data) {
            _this.selectDealer = data;
            console.log("dealers list", _this.selectDealer);
            // toast.dismiss();
        }, function (error) {
            console.log(error);
        });
    };
    FastagListPage.prototype.getmodel = function (d) {
        var _this = this;
        if (d.length > 0) {
            var pModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__msg_utility_msg_utility__["a" /* MsgUtilityPage */], {
                param: d
            });
            pModal.onDidDismiss(function (data) {
                console.log("chff", data);
                _this.selectus = data;
                _this.cond = true;
            });
            pModal.present();
        }
        else {
            var toast = this.toastCtrl.create({
                message: 'Dealers Not Found!',
                duration: 1300,
                position: "bottom"
            });
            toast.present();
        }
    };
    FastagListPage.prototype.executeThis = function () {
        this.selectus = [];
    };
    FastagListPage.prototype.sendmsg = function () {
        var _this = this;
        var data = {
            "messageTitle": this.titleMsg,
            "msgContent": this.fullMsg,
            "notificationType": "NOTIFICATION",
            "numberArr": this.selectus,
        };
        this.apiCall.startLoading().present();
        this.apiCall.announcementData(data)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
        });
    };
    FastagListPage.prototype.getList = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + 'fastTag/getRequest?id=' + this.islogin.supAdmin + '&role=supAdmin';
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('respData: ', respData);
            if (respData.length > 0) {
                _this.fastagList = respData;
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagListPage.prototype.dealerOnChnage = function (dealer) {
        console.log(dealer);
        console.log(dealer.dealer_id);
        this.dealerId = dealer.dealer_id;
        this.getDealercust();
    };
    FastagListPage.prototype.getDealercust = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getCustomer?uid=' + this.dealerId + '&pageNo=' + this.page + '&size=' + this.limit;
        //this.apiCall.startLoading().present();
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            console.log("first click initial", _this.DCustomerArraySearch);
            //this.apiCall.stopLoading();
            _this.CustomerDataa = data;
            _this.DCustomerArraySearch = [];
            _this.DCustomerArraySearch = _this.CustomerDataa;
            console.log("first click", _this.DCustomerArraySearch);
            _this.getmodel(_this.DCustomerArraySearch);
        }, function (err) {
            _this.apiCall.stopLoading();
            var a = JSON.parse(err._body);
            var b = a.message;
            var toast = _this.toastCtrl.create({
                message: b,
                duration: 2000,
                position: "bottom"
            });
            toast.present();
        });
    };
    FastagListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag-list',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/flying_gps_1/src/pages/fastag-list/fastag-list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{"Message Utility" | translate}}</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <span>SEND TO *:</span>&nbsp;&nbsp;&nbsp;\n  <input (click)="getmodel(DealerArraySearch)"\n         [checked]="cond"\n         id="male"\n         name="gender"\n         type="radio"\n         value="male" />\n  <label (click)="getmodel(DealerArraySearch)" for="male">Dealers</label>\n  <br />\n  <input (click)="getmodel(CustomerArraySearch)"\n         [checked]="cond"\n         id="female"\n         name="gender"\n         style="margin-left: 83px;"\n         type="radio"\n         value="female" />\n  <label (click)="getmodel(CustomerArraySearch)" for="female">\n    Direct Customer\n  </label>\n  <br />\n  <input [(ngModel)]="model"\n         id="other"\n         name="gender"\n         style="margin-left: 83px;"\n         type="radio"\n         value="other"(click)="executeThis()" />\n  <label for="other">Dealer\'s Customer</label>\n\n  <div *ngIf="model === \'other\'">\n    <ion-item style="padding: 0px;">\n      <ion-label style="margin-top: 15px;">\n        {{\'Select Dealers\' | translate}}\n      </ion-label>\n      <select-searchable (onChange)="dealerOnChnage(dealer_firstname)"\n                         [(ngModel)]="dealer_firstname"\n                         [canSearch]="true"\n                         [items]="selectDealer"\n                         item-content\n                         itemTextField="dealer_firstname"\n                         itemValueField="dealer_firstname"\n                         selectAll="true"\n                         style="padding: 0px;margin-top: -1%;">\n      </select-searchable>\n    </ion-item>\n  </div>\n\n  <div *ngIf="model !== \'other\'"></div>\n\n  <textarea [(ngModel)]="titleMsg"\n            placeholder="Enter the title of the message"\n            style="height: 55px;width: 100%;margin-top: 20px;">\n  </textarea>\n\n  <textarea [(ngModel)]="fullMsg"\n            placeholder="Enter the full message here"\n            style="height: 255px;width: 100%;margin-top: 20px;">\n  </textarea>\n  <ion-footer class="footSty">\n    <ion-toolbar>\n      <ion-row no-padding>\n        <ion-col style="text-align: center;" width-50>\n          <button (click)="sendmsg()" clear color="light" ion-button>\n            SEND\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-toolbar>\n  </ion-footer>\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/screenshots/flying_gps_1/src/pages/fastag-list/fastag-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FastagListPage);
    return FastagListPage;
}());

//# sourceMappingURL=fastag-list.js.map

/***/ })

});
//# sourceMappingURL=52.js.map