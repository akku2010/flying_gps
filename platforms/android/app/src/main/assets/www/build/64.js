webpackJsonp([64],{

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPageModule", function() { return CustomersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__customers__ = __webpack_require__(715);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




// import { AddDeviceModalPage } from './modals/add-device-modal';
var CustomersPageModule = /** @class */ (function () {
    function CustomersPageModule() {
    }
    CustomersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__customers__["a" /* CustomersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__customers__["a" /* CustomersPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
            // AddDeviceModalPage
            ]
        })
    ], CustomersPageModule);
    return CustomersPageModule;
}());

//# sourceMappingURL=customers.module.js.map

/***/ }),

/***/ 715:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__modals_report_setting_report_setting__ = __webpack_require__(437);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CustomersPage = /** @class */ (function () {
    function CustomersPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alerCtrl, events, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.events = events;
        this.translate = translate;
        this.CustomerArraySearch = [];
        this.page = 1;
        this.limit = 5;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.cust_Permission = this.islogin.cust_add_permission;
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        this.isDealer = this.islogin.isDealer;
    }
    CustomersPage.prototype.ngOnInit = function () {
        this.getcustomer();
    };
    CustomersPage.prototype.showAll = function (ev) {
        var _this = this;
        this.page = 1;
        console.log("show all clicked", ev.value);
        if (ev.value) {
            this.showAllCustmrs = true;
            var baseURLp = this.apiCall.mainUrl + "users/getCustomer?uid=" + this.islogin._id + "&pageNo=" + this.page + "&size=" + this.limit + "&all=true";
            // var baseURLp = this.apiCall.mainUrl + 'users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
            // this.apiCall.startLoading().present();
            this.apiCall.getCustomersCall(baseURLp)
                .subscribe(function (data) {
                // this.apiCall.stopLoading();
                _this.CustomerData = data;
                _this.CustomerArraySearch = [];
                _this.CustomerArraySearch = _this.CustomerData;
            }, function (err) {
                // this.apiCall.stopLoading();
                var a = JSON.parse(err._body);
                var b = a.message;
                var toast = _this.toastCtrl.create({
                    message: b,
                    duration: 2000,
                    position: "bottom"
                });
                toast.present();
                // toast.onDidDismiss(() => {
                //   this.navCtrl.setRoot('DashboardPage');
                // });
            });
        }
        else {
            this.showAllCustmrs = false;
            this.getcustomer();
        }
    };
    CustomersPage.prototype.reportSetting = function (item) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__modals_report_setting_report_setting__["a" /* ReportSettingModal */], {
            param: item
        });
        modal.onDidDismiss(function (data) {
            _this.navCtrl.setRoot("CustomersPage");
        });
        modal.present();
    };
    CustomersPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    CustomersPage.prototype.doRefresh = function (refresher) {
        this.getcustomer();
        refresher.complete();
    };
    CustomersPage.prototype.getItems = function (ev) {
        var val = ev.target.value.trim();
        this.CustomerArraySearch = this.CustomerArray.filter(function (item) {
            return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.CustomerArraySearch);
    };
    CustomersPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        console.log("search key: ", that.searchKey_string);
        debugger;
        if (that.searchKey_string === "" || that.searchKey_string === undefined) {
            setTimeout(function () {
                var baseURLp;
                if (that.showAllCustmrs) {
                    baseURLp = that.apiCall.mainUrl + "users/getCustomer?uid=" + that.islogin._id + "&pageNo=" + that.page + "&size=" + that.limit + "&all=true";
                }
                else {
                    baseURLp = that.apiCall.mainUrl + 'users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit;
                }
                that.ndata = [];
                _this.apiCall.getCustomersCall(baseURLp)
                    .subscribe(function (data) {
                    that.ndata = data;
                    for (var i = 0; i < that.ndata.length; i++) {
                        that.CustomerData.push(that.ndata[i]);
                    }
                    that.CustomerArraySearch = [];
                    that.CustomerArraySearch = that.CustomerData;
                }, function (err) {
                    _this.apiCall.stopLoading();
                });
                infiniteScroll.complete();
            }, 500);
        }
        else {
            setTimeout(function () {
                var baseURLp;
                if (that.showAllCustmrs) {
                    baseURLp = _this.apiCall.mainUrl + "users/getCustomer?uid=" + that.islogin._id + "&pageNo=" + that.page + "&size=" + that.limit + '&search=' + that.searchKey_string + "&all=true";
                }
                else {
                    baseURLp = _this.apiCall.mainUrl + 'users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&search=' + that.searchKey_string;
                }
                that.ndata = [];
                _this.apiCall.getCustomersCall(baseURLp)
                    .subscribe(function (data) {
                    that.ndata = data;
                    for (var i = 0; i < that.ndata.length; i++) {
                        that.CustomerData.push(that.ndata[i]);
                    }
                    that.CustomerArraySearch = [];
                    that.CustomerArraySearch = that.CustomerData;
                }, function (err) {
                    _this.apiCall.stopLoading();
                });
                infiniteScroll.complete();
            }, 500);
        }
    };
    CustomersPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        this.page = 1;
        var that = this;
        var baseURLp;
        if (that.showAllCustmrs) {
            baseURLp = this.apiCall.mainUrl + "users/getCustomer?uid=" + that.islogin._id + "&pageNo=" + that.page + "&size=" + that.limit + '&search=' + searchKey + "&all=true";
        }
        else {
            baseURLp = this.apiCall.mainUrl + 'users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&search=' + searchKey;
        }
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            that.CustomerArraySearch = data;
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    CustomersPage.prototype.onClear = function (ev) {
        var _this = this;
        this.page = 1;
        // this.getcustomer();
        ev.target.value = '';
        if (this.showAllCustmrs) {
            this.showAllCustmrs = true;
            var baseURLp = this.apiCall.mainUrl + "users/getCustomer?uid=" + this.islogin._id + "&pageNo=" + this.page + "&size=" + this.limit + "&all=true";
            // var baseURLp = this.apiCall.mainUrl + 'users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
            // this.apiCall.startLoading().present();
            this.apiCall.getCustomersCall(baseURLp)
                .subscribe(function (data) {
                // this.apiCall.stopLoading();
                _this.CustomerData = data;
                _this.CustomerArraySearch = [];
                _this.CustomerArraySearch = _this.CustomerData;
            }, function (err) {
                // this.apiCall.stopLoading();
                var a = JSON.parse(err._body);
                var b = a.message;
                var toast = _this.toastCtrl.create({
                    message: b,
                    duration: 2000,
                    position: "bottom"
                });
                toast.present();
                // toast.onDidDismiss(() => {
                //   this.navCtrl.setRoot('DashboardPage');
                // });
            });
        }
        else {
            this.showAllCustmrs = false;
            this.getcustomer();
        }
    };
    CustomersPage.prototype.getcustomer = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
        this.apiCall.startLoading().present();
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.CustomerData = data;
            _this.CustomerArraySearch = [];
            _this.CustomerArraySearch = _this.CustomerData;
        }, function (err) {
            _this.apiCall.stopLoading();
            var a = JSON.parse(err._body);
            var b = a.message;
            var toast = _this.toastCtrl.create({
                message: b,
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            // toast.onDidDismiss(() => {
            //   this.navCtrl.setRoot('DashboardPage');
            // });
        });
    };
    CustomersPage.prototype.CustomerStatus = function (Customersdeta) {
        var _this = this;
        var msg;
        if (Customersdeta.status) {
            msg = this.translate.instant('deactivateDealer', { value: this.translate.instant('cust') });
        }
        else {
            msg = this.translate.instant('activateDealer', { value: this.translate.instant('cust') });
        }
        var alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                    text: this.translate.instant('Yes'),
                    handler: function () {
                        _this.user_status(Customersdeta);
                    }
                },
                {
                    text: this.translate.instant('NO'),
                    handler: function () {
                        _this.getcustomer();
                    }
                }]
        });
        alert.present();
    };
    CustomersPage.prototype.user_status = function (Customersdeta) {
        var _this = this;
        var stat;
        if (Customersdeta.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var data = {
            "uId": Customersdeta._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant('Customer updated!!'),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                _this.getcustomer();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    CustomersPage.prototype.openupdateCustomersModal = function (Customersdetails) {
        var _this = this;
        this.customer = Customersdetails;
        var modal = this.modalCtrl.create('UpdateCustModalPage', {
            param: this.customer
        });
        modal.onDidDismiss(function (data) {
            _this.navCtrl.setRoot("CustomersPage");
        });
        modal.present();
    };
    CustomersPage.prototype.openAdddeviceModal = function (Customersdetails) {
        var _this = this;
        this.customer = Customersdetails;
        var profileModal = this.modalCtrl.create('AddDeviceModalPage', { custDet: this.customer });
        profileModal.onDidDismiss(function (data) {
            _this.getcustomer();
        });
        profileModal.present();
    };
    CustomersPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CustomersPage.prototype.openAddCustomerModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AddCustomerModal');
        modal.onDidDismiss(function () {
            _this.getcustomer();
        });
        modal.present();
    };
    CustomersPage.prototype.switchUser = function (cust_id) {
        var _this = this;
        localStorage.setItem('isDealervalue', 'true');
        localStorage.setItem('dealer', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'ON');
        localStorage.setItem('dealer_status', 'OFF');
        this.apiCall.getcustToken(cust_id)
            .subscribe(function (res) {
            var custToken = res;
            var logindata = JSON.stringify(custToken);
            var logindetails = JSON.parse(logindata);
            var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
            var details = JSON.parse(userDetails);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));
            var dealerSwitchObj = {
                "logindata": logindata,
                "details": userDetails,
                'condition_chk': details.isDealer
            };
            var temp = localStorage.getItem('isDealervalue');
            _this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
            _this.events.publish("sidemenu:event", temp);
            _this.navCtrl.setRoot('DashboardPage');
        }, function (err) {
            console.log(err);
        });
    };
    CustomersPage.prototype.DelateCustomer = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: this.translate.instant('Do you want to delete this customer?'),
            buttons: [{
                    text: this.translate.instant('NO')
                },
                {
                    text: this.translate.instant('Yes'),
                    handler: function () {
                        _this.deleteCus(_id);
                    }
                }]
        });
        alert.present();
    };
    CustomersPage.prototype.deleteCus = function (_id) {
        var _this = this;
        var data = {
            "userId": _id,
            'deleteuser': true
        };
        this.apiCall.deleteCustomerCall(data).
            subscribe(function (data) {
            var toast = _this.toastCtrl.create({
                message: _this.translate.instant("Deleted successfully!!"),
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                _this.getcustomer();
            });
            toast.present();
        }, function (err) {
            console.log(err);
        });
    };
    CustomersPage.prototype.onClickChat = function (item) {
        this.navCtrl.push('ChatPage', {
            params: item
        });
    };
    CustomersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/flying_gps_1/src/pages/customers/customers.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "Customers" | translate }}\n\n    </ion-title>\n\n\n\n    <ion-buttons end style="margin-top: -10px;"\n\n      *ngIf="(cust_Permission == undefined || cust_Permission == true) || isSuperAdminStatus">\n\n      <ion-item style="background: transparent;" class="myItem">\n\n        <ion-toggle color="gpsc" style="padding-top: 28px;" [(ngModel)]="showAllCustmrs" (ionChange)="showAll($event)"></ion-toggle>\n\n      </ion-item>\n\n    </ion-buttons>\n\n    <ion-buttons end *ngIf="(cust_Permission == undefined || cust_Permission == true) || isSuperAdminStatus">\n\n      <button ion-button icon-only (click)="openAddCustomerModal()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)" [(ngModel)]="searchKey_string"></ion-searchbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="{{\'Pull to refresh\' | translate}}"\n\n      refreshingSpinner="circles" refreshingText="{{\'Refreshing...\' | translate}}">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list>\n\n    <div *ngFor="let item of CustomerArraySearch">\n\n      <ion-item>\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/user.png">\n\n\n\n          <ion-row style="margin-top: 37%;font-size: 20px;">\n\n            <ion-col col-4>\n\n              <ion-icon name="trash" style="color: #b9002f;" (tap)="DelateCustomer(item._id)"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-1></ion-col>\n\n            <ion-col col-4>\n\n              <ion-icon name="clipboard" style="color: #e48099;" (tap)="reportSetting(item)"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-1></ion-col>\n\n          </ion-row>\n\n        </ion-thumbnail>\n\n        <div (tap)="switchUser(item._id)">\n\n          <p>\n\n            <span ion-text color="dark">{{ "Name:" | translate }} </span> {{item.first_name}}&nbsp;{{item.last_name}}\n\n          </p>\n\n          <p>\n\n            <span ion-text color="dark">{{ "Email:" | translate }} </span> {{item.email}}</p>\n\n          <p>\n\n            <span ion-text color="dark">{{ "Phone:" | translate }} </span>\n\n            <a href="tel:{{item.phone}}">{{item.phone}}</a>\n\n            <!-- <button ion-button color="light" (tap)="dialNumber(item.phone)"><ion-icon name="call"></ion-icon>&nbsp;{{item.phone}}</button> -->\n\n          </p>\n\n          <p>\n\n            <span ion-text color="dark">{{ "Password:" | translate }} </span>{{item.pass}}</p>\n\n          <p>\n\n            <span ion-text color="dark">{{ "Created On:" | translate }} </span>{{item.created_on | date:\'short\'}}</p>\n\n          <p>\n\n            <span ion-text color="dark">{{ "Total Vehicles:" | translate }} </span>\n\n            <span ion-text color="danger">{{item.total_vehicle}}</span>\n\n          </p>\n\n        </div>\n\n        <p>\n\n          <button ion-button small (click)="openupdateCustomersModal(item)">{{ "Edit" | translate }}</button>\n\n          <button ion-button small (click)="openAdddeviceModal(item)">{{ "Add Vehicle" | translate }}</button>\n\n          <button ion-button small (click)="CustomerStatus(item)"\n\n            *ngIf="item.status == true">{{ "Active" | translate }}</button>\n\n          <button ion-button small color="danger" (click)="CustomerStatus(item)"\n\n            *ngIf="item.status != true">{{ "InActive" | translate }}</button>\n\n          <button ion-button small (click)="onClickChat(item)">{{ "Chat" | translate }}</button>\n\n        </p>\n\n      </ion-item>\n\n    </div>\n\n\n\n  </ion-list>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="{{ \'Loading more data...\' | translate}}">\n\n    </ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/flying_gps_1/src/pages/customers/customers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["c" /* TranslateService */]])
    ], CustomersPage);
    return CustomersPage;
}());

//# sourceMappingURL=customers.js.map

/***/ })

});
//# sourceMappingURL=64.js.map